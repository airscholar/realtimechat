# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :chatapp, ChatappWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Ecm3MN9qg+soQaJK9jjnXoypMiKxAzYR4krFN9gJXKiL0Adk+QTXKOIv14lfb8ri",
  render_errors: [view: ChatappWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Chatapp.PubSub,
  live_view: [signing_salt: "6lbg24rR"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
